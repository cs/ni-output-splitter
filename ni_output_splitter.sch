EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2750 1450 2    50   ~ 0
P0.30
Text Label 2750 1550 2    50   ~ 0
P0.28
Text Label 2750 1650 2    50   ~ 0
P0.25
Text Label 2750 1750 2    50   ~ 0
DGND
Text Label 2750 1850 2    50   ~ 0
P0.22
Text Label 3250 1750 0    50   ~ 0
P0.23
Text Label 3250 1650 0    50   ~ 0
P0.24
Text Label 2050 3150 0    50   ~ 0
P0.0
Text Label 1550 3050 2    50   ~ 0
P0.1
Text Label 2050 2850 0    50   ~ 0
P0.2
Text Label 2050 2650 0    50   ~ 0
P0.3
Text Label 1550 3250 2    50   ~ 0
P0.4
Text Label 2050 3050 0    50   ~ 0
P0.5
Text Label 1550 2950 2    50   ~ 0
P0.6
Text Label 2050 2750 0    50   ~ 0
P0.7
Text Label 3250 3150 0    50   ~ 0
P0.8
Text Label 2750 3050 2    50   ~ 0
P0.9
Text Label 3250 2850 0    50   ~ 0
P0.10
Text Label 3250 2650 0    50   ~ 0
P0.11
Text Label 2750 3250 2    50   ~ 0
P0.12
Text Label 3250 3050 0    50   ~ 0
P0.13
Text Label 2750 2950 2    50   ~ 0
P0.14
Text Label 3250 2750 0    50   ~ 0
P0.15
Text Label 2750 2450 2    50   ~ 0
P0.16
Text Label 2750 2350 2    50   ~ 0
P0.17
Text Label 3250 2250 0    50   ~ 0
P0.18
Text Label 3250 2150 0    50   ~ 0
P0.19
Text Label 3250 2050 0    50   ~ 0
P0.20
Text Label 2750 1950 2    50   ~ 0
P0.21
Text Label 3250 2450 0    50   ~ 0
P0.26
Text Label 3250 2550 0    50   ~ 0
P0.27
Text Label 3250 1950 0    50   ~ 0
P0.29
Text Label 3250 1850 0    50   ~ 0
P0.31
Text Label 2050 4750 0    50   ~ 0
AI0
Text Label 1550 4750 2    50   ~ 0
AI8
Text Label 1550 4650 2    50   ~ 0
AI1
Text Label 1550 4450 2    50   ~ 0
AI10
Text Label 1550 4350 2    50   ~ 0
AI3
Text Label 1550 4150 2    50   ~ 0
AI4
Text Label 1550 3950 2    50   ~ 0
AI13
Text Label 1550 3850 2    50   ~ 0
AI6
Text Label 1550 3650 2    50   ~ 0
AI15
Text Label 2050 4550 0    50   ~ 0
AI9
Text Label 2050 4450 0    50   ~ 0
AI2
Text Label 2050 4250 0    50   ~ 0
AI11
Text Label 2050 4050 0    50   ~ 0
AI12
Text Label 2050 3950 0    50   ~ 0
AI5
Text Label 2050 3750 0    50   ~ 0
AI14
Text Label 2050 3650 0    50   ~ 0
AI7
Text Label 2050 3550 0    50   ~ 0
AIGND
Text Label 1550 3750 2    50   ~ 0
AIGND
Text Label 2050 4150 0    50   ~ 0
AISENSE1
Text Label 2050 3850 0    50   ~ 0
AIGND
Text Label 2050 4350 0    50   ~ 0
AIGND
Text Label 2050 4650 0    50   ~ 0
AIGND
Text Label 1550 4050 2    50   ~ 0
AIGND
Text Label 1550 4250 2    50   ~ 0
AIGND
Text Label 1550 4550 2    50   ~ 0
AIGND
Text Label 1550 3550 2    50   ~ 0
AO0
Text Label 1550 3450 2    50   ~ 0
AO1
Text Label 1550 3350 2    50   ~ 0
APFI0
Text Label 2050 3450 0    50   ~ 0
AOGND
Text Label 2050 3350 0    50   ~ 0
AOGND
Text Label 2050 3250 0    50   ~ 0
DGND
Text Label 3250 4750 0    50   ~ 0
AI16
Text Label 3250 4550 0    50   ~ 0
AI25
Text Label 3250 4450 0    50   ~ 0
AI18
Text Label 3250 4250 0    50   ~ 0
AI27
Text Label 3250 4150 0    50   ~ 0
AISENSE2
Text Label 3250 4050 0    50   ~ 0
AI28
Text Label 3250 3950 0    50   ~ 0
AI21
Text Label 3250 3750 0    50   ~ 0
AI30
Text Label 3250 3650 0    50   ~ 0
AI23
Text Label 2750 4750 2    50   ~ 0
AI24
Text Label 2750 4650 2    50   ~ 0
AI17
Text Label 2750 4450 2    50   ~ 0
AI26
Text Label 2750 4350 2    50   ~ 0
AI19
Text Label 2750 4150 2    50   ~ 0
AI20
Text Label 2750 3950 2    50   ~ 0
AI29
Text Label 2750 3850 2    50   ~ 0
AI22
Text Label 2750 3650 2    50   ~ 0
AI31
Text Label 2750 3550 2    50   ~ 0
AO2
Text Label 2750 3450 2    50   ~ 0
AO3
Text Label 2750 3350 2    50   ~ 0
APFI1
Text Label 3250 3450 0    50   ~ 0
AOGND
Text Label 3250 3350 0    50   ~ 0
AOGND
Text Label 3250 3850 0    50   ~ 0
AIGND
Text Label 3250 4350 0    50   ~ 0
AIGND
Text Label 3250 4650 0    50   ~ 0
AIGND
Text Label 2750 4550 2    50   ~ 0
AIGND
Text Label 2750 4250 2    50   ~ 0
AIGND
Text Label 2750 4050 2    50   ~ 0
AIGND
Text Label 2750 3750 2    50   ~ 0
AIGND
Text Label 3250 3550 0    50   ~ 0
AIGND
Text Label 2750 2150 2    50   ~ 0
+5V2
Text Label 2750 2750 2    50   ~ 0
+5V2
Text Label 3250 1450 0    50   ~ 0
DGND
Text Label 3250 1550 0    50   ~ 0
DGND
Text Label 3250 2350 0    50   ~ 0
DGND
Text Label 3250 2950 0    50   ~ 0
DGND
Text Label 3250 3250 0    50   ~ 0
DGND
Text Label 2750 2050 2    50   ~ 0
DGND
Text Label 2750 2250 2    50   ~ 0
DGND
Text Label 2750 2550 2    50   ~ 0
DGND
Text Label 2750 2650 2    50   ~ 0
DGND
Text Label 2750 2850 2    50   ~ 0
DGND
Text Label 2750 3150 2    50   ~ 0
DGND
Text Label 2050 1650 0    50   ~ 0
P2.0
Text Label 2050 1750 0    50   ~ 0
P1.7
Text Label 2050 1850 0    50   ~ 0
P2.7
Text Label 2050 1950 0    50   ~ 0
P2.5
Text Label 2050 2050 0    50   ~ 0
P1.4
Text Label 2050 2150 0    50   ~ 0
P1.3
Text Label 2050 2250 0    50   ~ 0
P1.2
Text Label 2050 2450 0    50   ~ 0
P2.2
Text Label 2050 2550 0    50   ~ 0
P2.3
Text Label 1550 1450 2    50   ~ 0
P2.6
Text Label 1550 1550 2    50   ~ 0
P2.4
Text Label 1550 1650 2    50   ~ 0
P2.1
Text Label 1550 1850 2    50   ~ 0
P1.6
Text Label 1550 1950 2    50   ~ 0
P1.5
Text Label 1550 2150 2    50   ~ 0
+5V1
Text Label 1550 2350 2    50   ~ 0
P1.1
Text Label 1550 2450 2    50   ~ 0
P1.0
Text Label 1550 2750 2    50   ~ 0
+5V1
Text Label 2050 1450 0    50   ~ 0
DGND
Text Label 2050 1550 0    50   ~ 0
DGND
Text Label 2050 2350 0    50   ~ 0
DGND
Text Label 2050 2950 0    50   ~ 0
DGND
Text Label 1550 1750 2    50   ~ 0
DGND
Text Label 1550 2050 2    50   ~ 0
DGND
Text Label 1550 2250 2    50   ~ 0
DGND
Text Label 1550 2550 2    50   ~ 0
DGND
Text Label 1550 2650 2    50   ~ 0
DGND
Text Label 1550 2850 2    50   ~ 0
DGND
Text Label 1550 3150 2    50   ~ 0
DGND
Text Notes 1300 1150 0    50   ~ 0
Output from NI-6353\nConnectors: TE 2-5174225-5
Wire Notes Line
	1250 950  1250 5250
Wire Notes Line
	1250 5250 3650 5250
Wire Notes Line
	3650 5250 3650 950 
Wire Notes Line
	3650 950  1250 950 
$Comp
L Connector:Conn_Coaxial J7
U 1 1 5F34F7E0
P 10300 800
F 0 "J7" H 10400 775 50  0000 L CNN
F 1 "Conn_Coaxial" H 10400 684 50  0000 L CNN
F 2 "footprints:P_BNC226978" H 10300 800 50  0001 C CNN
F 3 " ~" H 10300 800 50  0001 C CNN
	1    10300 800 
	1    0    0    -1  
$EndComp
Text Label 10100 800  2    50   ~ 0
AO0
Text Label 10100 1050 2    50   ~ 0
AGND
Wire Wire Line
	10100 1050 10300 1050
Wire Wire Line
	10300 1050 10300 1000
$Comp
L Connector:Conn_Coaxial J8
U 1 1 5F351D87
P 10300 1250
F 0 "J8" H 10400 1225 50  0000 L CNN
F 1 "Conn_Coaxial" H 10400 1134 50  0000 L CNN
F 2 "footprints:P_BNC226978" H 10300 1250 50  0001 C CNN
F 3 " ~" H 10300 1250 50  0001 C CNN
	1    10300 1250
	1    0    0    -1  
$EndComp
Text Label 10100 1250 2    50   ~ 0
AO1
Text Label 10100 1500 2    50   ~ 0
AGND
Wire Wire Line
	10100 1500 10300 1500
Wire Wire Line
	10300 1500 10300 1450
$Comp
L Connector:Conn_Coaxial J9
U 1 1 5F352DFE
P 10300 1700
F 0 "J9" H 10400 1675 50  0000 L CNN
F 1 "Conn_Coaxial" H 10400 1584 50  0000 L CNN
F 2 "footprints:P_BNC226978" H 10300 1700 50  0001 C CNN
F 3 " ~" H 10300 1700 50  0001 C CNN
	1    10300 1700
	1    0    0    -1  
$EndComp
Text Label 10100 1700 2    50   ~ 0
AO2
Text Label 10100 1950 2    50   ~ 0
AGND
Wire Wire Line
	10100 1950 10300 1950
Wire Wire Line
	10300 1950 10300 1900
$Comp
L Connector:Conn_Coaxial J10
U 1 1 5F352E08
P 10300 2150
F 0 "J10" H 10400 2125 50  0000 L CNN
F 1 "Conn_Coaxial" H 10400 2034 50  0000 L CNN
F 2 "footprints:P_BNC226978" H 10300 2150 50  0001 C CNN
F 3 " ~" H 10300 2150 50  0001 C CNN
	1    10300 2150
	1    0    0    -1  
$EndComp
Text Label 10100 2150 2    50   ~ 0
AO3
Text Label 10100 2400 2    50   ~ 0
AGND
Wire Wire Line
	10100 2400 10300 2400
Wire Wire Line
	10300 2400 10300 2350
$Comp
L Connector:Conn_Coaxial J11
U 1 1 5F353E56
P 10300 2600
F 0 "J11" H 10400 2575 50  0000 L CNN
F 1 "Conn_Coaxial" H 10400 2484 50  0000 L CNN
F 2 "footprints:P_BNC226978" H 10300 2600 50  0001 C CNN
F 3 " ~" H 10300 2600 50  0001 C CNN
	1    10300 2600
	1    0    0    -1  
$EndComp
Text Label 10100 2600 2    50   ~ 0
APFI0
Text Label 10100 2850 2    50   ~ 0
AGND
Wire Wire Line
	10100 2850 10300 2850
Wire Wire Line
	10300 2850 10300 2800
$Comp
L Connector:Conn_Coaxial J12
U 1 1 5F353E60
P 10300 3050
F 0 "J12" H 10400 3025 50  0000 L CNN
F 1 "Conn_Coaxial" H 10400 2934 50  0000 L CNN
F 2 "footprints:P_BNC226978" H 10300 3050 50  0001 C CNN
F 3 " ~" H 10300 3050 50  0001 C CNN
	1    10300 3050
	1    0    0    -1  
$EndComp
Text Label 10100 3050 2    50   ~ 0
APFI1
Text Label 10100 3300 2    50   ~ 0
AGND
Wire Wire Line
	10100 3300 10300 3300
Wire Wire Line
	10300 3300 10300 3250
$Comp
L Connector:DB37_Female_MountingHoles J6
U 1 1 5F35883D
P 8800 3850
F 0 "J6" H 8980 3852 50  0000 L CNN
F 1 "DB37_Female_MountingHoles" H 8980 3761 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-37_Female_Horizontal_P2.77x2.84mm_EdgePinOffset7.70mm_Housed_MountingHolesOffset9.12mm" H 8800 3850 50  0001 C CNN
F 3 " ~" H 8800 3850 50  0001 C CNN
	1    8800 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB25_Female_MountingHoles J3
U 1 1 5F35D910
P 4650 2300
F 0 "J3" H 4600 3850 50  0000 L CNN
F 1 "DB25_Female_MountingHoles" H 4100 3700 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Vertical_P2.77x2.84mm_MountingHoles" H 4650 2300 50  0001 C CNN
F 3 " ~" H 4650 2300 50  0001 C CNN
	1    4650 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB25_Female_MountingHoles J5
U 1 1 5F35FC56
P 5900 2300
F 0 "J5" H 5850 3800 50  0000 C CNN
F 1 "DB25_Female_MountingHoles" H 5350 3700 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Vertical_P2.77x2.84mm_MountingHoles" H 5900 2300 50  0001 C CNN
F 3 " ~" H 5900 2300 50  0001 C CNN
	1    5900 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB25_Female_MountingHoles J4
U 1 1 5F3616F6
P 7550 2300
F 0 "J4" H 7500 3800 50  0000 L CNN
F 1 "DB25_Female_MountingHoles" H 7000 3700 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Vertical_P2.77x2.84mm_MountingHoles" H 7550 2300 50  0001 C CNN
F 3 " ~" H 7550 2300 50  0001 C CNN
	1    7550 2300
	1    0    0    -1  
$EndComp
Text Label 8500 2050 2    50   ~ 0
DGND
Text Label 8500 5650 2    50   ~ 0
P0.0
Text Label 8500 5550 2    50   ~ 0
P0.1
Text Label 8500 5450 2    50   ~ 0
P0.2
Text Label 8500 5350 2    50   ~ 0
P0.3
Text Label 8500 5250 2    50   ~ 0
P0.4
Text Label 8500 5150 2    50   ~ 0
P0.5
Text Label 8500 5050 2    50   ~ 0
P0.6
Text Label 8500 4950 2    50   ~ 0
P0.7
Text Label 8500 4850 2    50   ~ 0
P0.8
Text Label 8500 4750 2    50   ~ 0
P0.9
Text Label 8500 4650 2    50   ~ 0
P0.10
Text Label 8500 4550 2    50   ~ 0
P0.11
Text Label 8500 4450 2    50   ~ 0
P0.12
Text Label 8500 4350 2    50   ~ 0
P0.13
Text Label 8500 4250 2    50   ~ 0
P0.14
Text Label 8500 4150 2    50   ~ 0
P0.15
Text Label 8500 4050 2    50   ~ 0
P0.16
Text Label 8500 3950 2    50   ~ 0
P0.17
Text Label 8500 3850 2    50   ~ 0
P0.18
Text Label 8500 3750 2    50   ~ 0
P0.19
Text Label 8500 3650 2    50   ~ 0
P0.20
Text Label 8500 3550 2    50   ~ 0
P0.21
Text Label 8500 3450 2    50   ~ 0
P0.22
Text Label 8500 3350 2    50   ~ 0
P0.23
Text Label 8500 3250 2    50   ~ 0
P0.24
Text Label 8500 3150 2    50   ~ 0
P0.25
Text Label 8500 3050 2    50   ~ 0
P0.26
Text Label 8500 2950 2    50   ~ 0
P0.27
Text Label 8500 2850 2    50   ~ 0
P0.28
Text Label 8500 2750 2    50   ~ 0
P0.29
Text Label 8500 2650 2    50   ~ 0
P0.30
Text Label 8500 2550 2    50   ~ 0
P0.31
Text Label 8500 2450 2    50   ~ 0
DGND
Text Label 8500 2350 2    50   ~ 0
DGND
$Comp
L Connector_Generic_Shielded:Conn_02x34_Top_Bottom_Shielded J2
U 1 1 5F36CF4E
P 2950 3050
F 0 "J2" H 3000 4865 50  0000 C CNN
F 1 "Conn_02x34_Top_Bottom_Shielded" H 3000 4774 50  0000 C CNN
F 2 "footprints:TYCO_2-174225-5" H 2950 3050 50  0001 C CNN
F 3 "~" H 2950 3050 50  0001 C CNN
	1    2950 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic_Shielded:Conn_02x34_Top_Bottom_Shielded J1
U 1 1 5F37951B
P 1750 3050
F 0 "J1" H 1800 4865 50  0000 C CNN
F 1 "Conn_02x34_Top_Bottom_Shielded" H 1800 4774 50  0000 C CNN
F 2 "footprints:TYCO_2-174225-5" H 1750 3050 50  0001 C CNN
F 3 "~" H 1750 3050 50  0001 C CNN
	1    1750 3050
	1    0    0    -1  
$EndComp
Text Label 1700 5050 2    50   ~ 0
SHIELD
Wire Wire Line
	1700 5050 1800 5050
Wire Wire Line
	1800 5050 1800 4950
Text Label 2900 5050 2    50   ~ 0
SHIELD
Wire Wire Line
	2900 5050 3000 5050
Wire Wire Line
	3000 5050 3000 4950
Text Label 5800 3800 2    50   ~ 0
SHIELD
Wire Wire Line
	5800 3800 5900 3800
Wire Wire Line
	5900 3800 5900 3700
Text Label 4550 3800 2    50   ~ 0
SHIELD
Wire Wire Line
	4550 3800 4650 3800
Wire Wire Line
	4650 3800 4650 3700
Text Label 7450 3800 2    50   ~ 0
SHIELD
Wire Wire Line
	7450 3800 7550 3800
Wire Wire Line
	7550 3800 7550 3700
Text Label 8700 5950 2    50   ~ 0
SHIELD
Wire Wire Line
	8700 5950 8800 5950
Wire Wire Line
	8800 5950 8800 5850
Text Notes 8300 1850 0    50   ~ 0
Output for\nopto-coupler\nboxes
Text Label 4350 1100 2    50   ~ 0
AI0
Text Label 4350 1200 2    50   ~ 0
AI8
Text Label 4350 1400 2    50   ~ 0
AI1
Text Label 4350 1500 2    50   ~ 0
AI9
Text Label 4350 1700 2    50   ~ 0
AI2
Text Label 4350 1800 2    50   ~ 0
AI10
Text Label 4350 2000 2    50   ~ 0
AI3
Text Label 4350 2100 2    50   ~ 0
AI11
Text Label 4350 2400 2    50   ~ 0
AI4
Text Label 4350 2500 2    50   ~ 0
AI12
Text Label 4350 2300 2    50   ~ 0
AISENSE1
Text Label 4350 2700 2    50   ~ 0
AI5
Text Label 4350 2800 2    50   ~ 0
AI13
Text Label 4350 3000 2    50   ~ 0
AI6
Text Label 4350 3100 2    50   ~ 0
AI14
Text Label 4350 3300 2    50   ~ 0
AI7
Text Label 4350 3400 2    50   ~ 0
AI15
Text Label 4350 1300 2    50   ~ 0
AIGND
Text Label 4350 1600 2    50   ~ 0
AIGND
Text Label 4350 1900 2    50   ~ 0
AIGND
Text Label 4350 2200 2    50   ~ 0
AIGND
Text Label 4350 2600 2    50   ~ 0
AIGND
Text Label 4350 2900 2    50   ~ 0
AIGND
Text Label 4350 3200 2    50   ~ 0
AIGND
Text Label 4350 3500 2    50   ~ 0
AIGND
Text Label 5600 1100 2    50   ~ 0
AI16
Text Label 5600 1200 2    50   ~ 0
AI24
Text Label 5600 2300 2    50   ~ 0
AISENSE2
Text Label 5600 1400 2    50   ~ 0
AI17
Text Label 5600 1500 2    50   ~ 0
AI25
Text Label 5600 1700 2    50   ~ 0
AI18
Text Label 5600 1800 2    50   ~ 0
AI26
Text Label 5600 2000 2    50   ~ 0
AI19
Text Label 5600 2100 2    50   ~ 0
AI27
Text Label 5600 2400 2    50   ~ 0
AI20
Text Label 5600 2500 2    50   ~ 0
AI28
Text Label 5600 2700 2    50   ~ 0
AI21
Text Label 5600 2800 2    50   ~ 0
AI29
Text Label 5600 3000 2    50   ~ 0
AI22
Text Label 5600 3100 2    50   ~ 0
AI30
Text Label 5600 3300 2    50   ~ 0
AI23
Text Label 5600 3400 2    50   ~ 0
AI31
Text Label 5600 1300 2    50   ~ 0
AIGND
Text Label 5600 1600 2    50   ~ 0
AIGND
Text Label 5600 1900 2    50   ~ 0
AIGND
Text Label 5600 2200 2    50   ~ 0
AIGND
Text Label 5600 2600 2    50   ~ 0
AIGND
Text Label 5600 2900 2    50   ~ 0
AIGND
Text Label 5600 3200 2    50   ~ 0
AIGND
Text Label 5600 3500 2    50   ~ 0
AIGND
Text Label 7250 1200 2    50   ~ 0
DGND
Text Label 7250 1400 2    50   ~ 0
DGND
Text Label 7250 1700 2    50   ~ 0
DGND
Text Label 7250 2300 2    50   ~ 0
DGND
Text Label 7250 2700 2    50   ~ 0
DGND
Text Label 7250 3300 2    50   ~ 0
DGND
Text Label 7250 3500 2    50   ~ 0
DGND
Text Label 7250 3000 2    50   ~ 0
DGND
Text Label 7250 3100 2    50   ~ 0
P1.0
Text Label 7250 2900 2    50   ~ 0
P1.1
Text Label 7250 2800 2    50   ~ 0
P1.2
Text Label 7250 2600 2    50   ~ 0
P1.3
Text Label 7250 2400 2    50   ~ 0
P1.4
Text Label 7250 2500 2    50   ~ 0
+5V1
Text Label 7250 2100 2    50   ~ 0
P1.5
Text Label 7250 1900 2    50   ~ 0
P1.6
Text Label 7250 1800 2    50   ~ 0
P1.7
Text Label 7250 1600 2    50   ~ 0
P2.0
Text Label 7250 1500 2    50   ~ 0
P2.1
Text Label 7250 3200 2    50   ~ 0
P2.2
Text Label 7250 3400 2    50   ~ 0
P2.3
Text Label 7250 1300 2    50   ~ 0
P2.4
Text Label 7250 2200 2    50   ~ 0
P2.5
Text Label 7250 1100 2    50   ~ 0
P2.6
Text Label 7250 2000 2    50   ~ 0
P2.7
$Comp
L Mechanical:MountingHole H1
U 1 1 5F3BBC6F
P 10500 5700
F 0 "H1" H 10600 5746 50  0000 L CNN
F 1 "MountingHole" H 10600 5655 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 10500 5700 50  0001 C CNN
F 3 "~" H 10500 5700 50  0001 C CNN
	1    10500 5700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F3BC5C4
P 10500 5900
F 0 "H2" H 10600 5946 50  0000 L CNN
F 1 "MountingHole" H 10600 5855 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 10500 5900 50  0001 C CNN
F 3 "~" H 10500 5900 50  0001 C CNN
	1    10500 5900
	1    0    0    -1  
$EndComp
Text Label 3600 7600 2    50   ~ 0
AGND
Text Label 3800 7600 0    50   ~ 0
AIGND
Wire Wire Line
	3800 7600 3700 7600
Text Label 3800 7400 0    50   ~ 0
AOGND
Wire Wire Line
	3800 7400 3700 7400
Wire Wire Line
	3700 7400 3700 7600
Connection ~ 3700 7600
Wire Wire Line
	3700 7600 3600 7600
$Comp
L Connector:DB25_Female_MountingHoles J13
U 1 1 5F33925B
P 4650 5600
F 0 "J13" H 4600 7150 50  0000 L CNN
F 1 "DB25_Female_MountingHoles" H 4100 7000 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Vertical_P2.77x2.84mm_MountingHoles" H 4650 5600 50  0001 C CNN
F 3 " ~" H 4650 5600 50  0001 C CNN
	1    4650 5600
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB25_Female_MountingHoles J14
U 1 1 5F339261
P 5900 5600
F 0 "J14" H 5850 7100 50  0000 C CNN
F 1 "DB25_Female_MountingHoles" H 5350 7000 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Vertical_P2.77x2.84mm_MountingHoles" H 5900 5600 50  0001 C CNN
F 3 " ~" H 5900 5600 50  0001 C CNN
	1    5900 5600
	1    0    0    -1  
$EndComp
Text Label 5800 7100 2    50   ~ 0
SHIELD
Wire Wire Line
	5800 7100 5900 7100
Wire Wire Line
	5900 7100 5900 7000
Text Label 4550 7100 2    50   ~ 0
SHIELD
Wire Wire Line
	4550 7100 4650 7100
Wire Wire Line
	4650 7100 4650 7000
Text Label 4350 4400 2    50   ~ 0
AI8
Text Label 4350 4700 2    50   ~ 0
AI9
Text Label 4350 5000 2    50   ~ 0
AI10
Text Label 4350 5300 2    50   ~ 0
AI11
Text Label 4350 5700 2    50   ~ 0
AI12
Text Label 4350 5600 2    50   ~ 0
AISENSE1
Text Label 4350 6000 2    50   ~ 0
AI13
Text Label 4350 6300 2    50   ~ 0
AI14
Text Label 4350 6600 2    50   ~ 0
AI15
Text Label 4350 4600 2    50   ~ 0
AIGND
Text Label 4350 4900 2    50   ~ 0
AIGND
Text Label 4350 5200 2    50   ~ 0
AIGND
Text Label 4350 5500 2    50   ~ 0
AIGND
Text Label 4350 5900 2    50   ~ 0
AIGND
Text Label 4350 6200 2    50   ~ 0
AIGND
Text Label 4350 6500 2    50   ~ 0
AIGND
Text Label 4350 6800 2    50   ~ 0
AIGND
Text Label 5600 4400 2    50   ~ 0
AI24
Text Label 5600 5600 2    50   ~ 0
AISENSE2
Text Label 5600 4700 2    50   ~ 0
AI25
Text Label 5600 5000 2    50   ~ 0
AI26
Text Label 5600 5300 2    50   ~ 0
AI27
Text Label 5600 5700 2    50   ~ 0
AI28
Text Label 5600 4600 2    50   ~ 0
AIGND
Text Label 5600 4900 2    50   ~ 0
AIGND
Text Label 5600 5200 2    50   ~ 0
AIGND
Text Label 5600 5500 2    50   ~ 0
AIGND
Text Label 5600 5900 2    50   ~ 0
AIGND
Text Label 5600 6200 2    50   ~ 0
AIGND
Text Label 5600 6500 2    50   ~ 0
AIGND
Text Label 5600 6800 2    50   ~ 0
AIGND
Text Label 5600 6000 2    50   ~ 0
AI29
Text Label 5600 6300 2    50   ~ 0
AI30
Text Label 5600 6600 2    50   ~ 0
AI31
Text Label 5600 6700 2    50   ~ 0
AIGND
Text Label 5600 6400 2    50   ~ 0
AIGND
Text Label 5600 6100 2    50   ~ 0
AIGND
Text Label 5600 5800 2    50   ~ 0
AIGND
Text Label 5600 5400 2    50   ~ 0
AIGND
Text Label 5600 5100 2    50   ~ 0
AIGND
Text Label 5600 4800 2    50   ~ 0
AIGND
Text Label 5600 4500 2    50   ~ 0
AIGND
Text Label 4350 4500 2    50   ~ 0
AIGND
Text Label 4350 4800 2    50   ~ 0
AIGND
Text Label 4350 5100 2    50   ~ 0
AIGND
Text Label 4350 5400 2    50   ~ 0
AIGND
Text Label 4350 5800 2    50   ~ 0
AIGND
Text Label 4350 6100 2    50   ~ 0
AIGND
Text Label 4350 6400 2    50   ~ 0
AIGND
Text Label 4350 6700 2    50   ~ 0
AIGND
$Comp
L Connector:USB_A J15
U 1 1 5FC9B03E
P 1750 5850
F 0 "J15" H 1807 6317 50  0000 C CNN
F 1 "USB_A" H 1807 6226 50  0000 C CNN
F 2 "Connector_USB:USB_A_Molex_105057_Vertical" H 1900 5800 50  0001 C CNN
F 3 " ~" H 1900 5800 50  0001 C CNN
	1    1750 5850
	1    0    0    -1  
$EndComp
Text Label 1550 6300 2    50   ~ 0
SHIELD
Wire Wire Line
	1550 6300 1650 6300
Wire Wire Line
	1650 6300 1650 6250
Text Label 1550 6400 2    50   ~ 0
DGND
Wire Wire Line
	1550 6400 1750 6400
Wire Wire Line
	1750 6400 1750 6250
NoConn ~ 2050 5850
NoConn ~ 2050 5950
Text Label 2150 5650 0    50   ~ 0
+5V1
Wire Wire Line
	2150 5650 2050 5650
$Comp
L Connector:USB_A J16
U 1 1 5FCA49F5
P 1750 6950
F 0 "J16" H 1807 7417 50  0000 C CNN
F 1 "USB_A" H 1807 7326 50  0000 C CNN
F 2 "Connector_USB:USB_A_Molex_105057_Vertical" H 1900 6900 50  0001 C CNN
F 3 " ~" H 1900 6900 50  0001 C CNN
	1    1750 6950
	1    0    0    -1  
$EndComp
Text Label 1550 7400 2    50   ~ 0
SHIELD
Wire Wire Line
	1550 7400 1650 7400
Wire Wire Line
	1650 7400 1650 7350
Text Label 1550 7500 2    50   ~ 0
DGND
Wire Wire Line
	1550 7500 1750 7500
Wire Wire Line
	1750 7500 1750 7350
NoConn ~ 2050 6950
NoConn ~ 2050 7050
Text Label 2150 6750 0    50   ~ 0
+5V1
Wire Wire Line
	2150 6750 2050 6750
$Comp
L Connector:USB_A J17
U 1 1 5FCAAB15
P 3000 5850
F 0 "J17" H 3057 6317 50  0000 C CNN
F 1 "USB_A" H 3057 6226 50  0000 C CNN
F 2 "Connector_USB:USB_A_Molex_105057_Vertical" H 3150 5800 50  0001 C CNN
F 3 " ~" H 3150 5800 50  0001 C CNN
	1    3000 5850
	1    0    0    -1  
$EndComp
Text Label 2800 6300 2    50   ~ 0
SHIELD
Wire Wire Line
	2800 6300 2900 6300
Wire Wire Line
	2900 6300 2900 6250
Text Label 2800 6400 2    50   ~ 0
DGND
Wire Wire Line
	2800 6400 3000 6400
Wire Wire Line
	3000 6400 3000 6250
NoConn ~ 3300 5850
NoConn ~ 3300 5950
Text Label 3400 5650 0    50   ~ 0
+5V2
Wire Wire Line
	3400 5650 3300 5650
$Comp
L Connector:USB_A J18
U 1 1 5FCAAB25
P 3000 6950
F 0 "J18" H 3057 7417 50  0000 C CNN
F 1 "USB_A" H 3057 7326 50  0000 C CNN
F 2 "Connector_USB:USB_A_Molex_105057_Vertical" H 3150 6900 50  0001 C CNN
F 3 " ~" H 3150 6900 50  0001 C CNN
	1    3000 6950
	1    0    0    -1  
$EndComp
Text Label 2800 7400 2    50   ~ 0
SHIELD
Wire Wire Line
	2800 7400 2900 7400
Wire Wire Line
	2900 7400 2900 7350
Text Label 2800 7500 2    50   ~ 0
DGND
Wire Wire Line
	2800 7500 3000 7500
Wire Wire Line
	3000 7500 3000 7350
NoConn ~ 3300 6950
NoConn ~ 3300 7050
Text Label 3400 6750 0    50   ~ 0
+5V2
Wire Wire Line
	3400 6750 3300 6750
$EndSCHEMATC
